(function($) {

  Drupal.behaviors.commerceExtlink = {
  
    attach: function (context, settings) {
    
      if (!settings.commerce_extlink.isPopupOverridden) {
      
        settings.commerce_extlink.isPopupOverridden = true;
        Drupal.extlink = Drupal.extlink || {};
        Drupal.extlink.popupClickHandler = function (e) {
        
          var message = false;
          if (settings.commerce_extlink.isCartEmpty) {
            if (settings.extlink.extAlert) {
              message = settings.extlink.extAlertText;
            }
          }
          else {
            if (settings.commerce_extlink.extAlert) {
              message = settings.commerce_extlink.extAlertText;
            }
          }
          if (message === false) {
            // Pass through as there's no message to display.
            return true;
          }
          else {
            return confirm(message);
          }
        }
        
      }
    }
  
  };

})(jQuery);
